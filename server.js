'use strict';

const express = require('express');

// Constants
const PORT = 5000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Site OK');
});
app.get('/health', (req, res) => {
  res.status(200).json({status:"sucesso"});
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);